package ch.tbz.Template.Template.src.ch.tbz.lib;

import java.util.Objects;

import static ch.tbz.lib.Input.*;

public class calculator {
    public static void main(String [] args) {
        boolean running = true;
        do {
            double num1 = inputDouble("Whats your first number?");
            double num2 = inputDouble("Whats your second number?");
            String operation = inputString("Which operation do you want to use? +, -, *, /");

            if (Objects.equals(operation, "+")){
                double result = num1 +num2;
                System.out.print("Your answer is ");
                System.out.printf("%.1f", result);
                System.out.println(" ");
                running = inputBoolean("Do you want to prosseed? yes, no");
            }
            else if(Objects.equals(operation, "-")){
                double result = num1 - num2;
                System.out.print("Your answer is ");
                System.out.printf("%.1f", result);
                System.out.println(" ");
                running = inputBoolean("Do you want to prosseed? yes, no");
            }
            else if(Objects.equals(operation, "*")){
                double result = num1 * num2;
                System.out.print("Your answer is ");
                System.out.printf("%.1f", result);
                System.out.println(" ");
                running = inputBoolean("Do you want to prosseed? yes, no");
            }
            else if(Objects.equals(operation, "/")){
                double result = num1 / num2;
                System.out.print("Your answer is ");
                System.out.printf("%.1f", result);
                System.out.println(" ");
                running = inputBoolean("Do you want to prosseed? yes, no");
            }
            else{
                System.out.println("Pleas enter an operation.");
            }
        }while (running);
        System.out.println("Thank you and goodbye!");
    }
}
