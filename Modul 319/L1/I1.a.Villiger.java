package ch.tbz.lib;
import java.util.Objects;

import static ch.tbz.lib.Input.*;

public class calculator {
    public static void main(String[] args){
    boolean running = true;
    do {
        double num1 = inputDouble("Geben Sie ihre erste Zahl ein.");
        double num2 = inputDouble("Geben Sie Ihre zweite Zahl ein.");
        String operation = inputString("Welche Operation möchten Sie verwenden?");

        if (Objects.equals(operation, "+")){
            double result = num1 + num2;
            System.out.print("Das Ergebnis ist: ");
            System.out.printf("%.1f", result);
            System.out.println(" ");
            running = inputBoolean("Wollen Sie fortfahren? yes, no");
        }
        else if (Objects.equals(operation,"-")){
            double result = num1 - num2;
            System.out.printf("%.1f", result);
            System.out.println(" ");
            running = inputBoolean("Wollen Sie fortfahren? yes, no");
        }
        else if (Objects.equals(operation,"*")){
            double result = num1 * num2;
            System.out.printf("%.1f", result);
            System.out.println(" ");
            running = inputBoolean("Wollen Sie fortfahren? yes, no");
        }
        else if (Objects.equals(operation,"/")){
            double result = num1 / num2;
            System.out.printf("%.1f", result);
            System.out.println(" ");
            running = inputBoolean("Wollen Sie fortfahren? yes, no");
            }
        }
    while (running);
    System.out.println("Vielen Dank und auf Wiedersehen!");
    }
}
