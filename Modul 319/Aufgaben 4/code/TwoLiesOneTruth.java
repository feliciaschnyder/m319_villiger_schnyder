package ch.tbz.Template.Template.src.ch.tbz.lib;

import static ch.tbz.lib.Input.inputBoolean;
import static ch.tbz.lib.Input.inputString;

import java.util.Objects;

public class TwoLiesOneTruth extends HauptklasseA4 {
    public static void gameStart() {
        String player1 = "";
        String player2 = "";
        String sen1 = "";
        String sen2 = "";
        String sen3 = "";
        int truth = 0;
        int guess = 0;
        String trueSentence = "";
        boolean running = true;

        do {
            //Game rules//
            System.out.println("Welcome to 2 Lies, 1 Truth! You have chosen the funniest of all games!");
            System.out.println("Game rules:");
            System.out.println("This game is made for two players. " +
                    "At the beginning, you can chose between Player One and Player Two. " +
                    "As Player One, you have to write down two lies and one truth. " +
                    "You have to make sure that your mate don't see your sentence." +
                    "As Player Two, you have to guess which sentence from Player One is the truth. " +
                    "Lets start with the game - have fun!");

            if (player1.isEmpty()) {
                System.out.println("Whats your name, Player One?");
                player1 = scanner.next();
            }
            if (player2.isEmpty()) {
                System.out.println("Whats your name, Player Two?");
                player2 = scanner.next();
            }
            if (player1 != "" && player2 != "") {
                sen1 = inputString(player1 + ", please write down your first sentence.");
                sen2 = inputString("Now write down your second sentence.");
                sen3 = inputString("And finally your third sentence.");
                System.out.println("Which of your sentences is the truth? (1,2,3)");
                truth = scanner.nextInt();
                if (truth == 1) {
                    trueSentence = sen1;
                } else if (truth == 2) {
                    trueSentence = sen2;
                } else if (truth == 3) {
                    trueSentence = sen3;
                } else {
                    running = inputBoolean("Error1. Something went wrong. Do you want to restart the game? (yes, no)");
                }
            }
            //for 30 empty lines
            for (int j = 0; j <= 30; j++) {
                System.out.println(" ");
            }
            System.out.println(player2 + ", now it's your turn!");
            System.out.println("Which sentence do you think is the truth?");
            System.out.println(sen1);
            System.out.println(sen2);
            System.out.println(sen3);
            guess = scanner.nextInt();

            //comparison of sentences
            if (Objects.equals(guess, truth)) {
                System.out.println("Yay, your thoughts were right! You know your mate very well!");
                running = inputBoolean("Do you want to restart the game? (yes, no)");
            } else if (guess != truth) {
                System.out.println("Oh no, you are wrong! '" + trueSentence + "' was the truth and the other sentences were lies!");
                running = inputBoolean("Do you want to restart the game? (yes, no)");
            } else {
                running = inputBoolean("Error2. Something went wrong. Do you want to restart the game? (yes, no)");
            }
        } while (running);
        System.out.println("Thank you and goodbye!");
    }
}
