package ch.tbz.Template.Template.src.ch.tbz.lib;

import java.util.Scanner;

public class HauptklasseA4 {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        boolean isRunning = true;
        String games;

        do {
            System.out.println("""
                                        
                    Welcome user which of our fantastic games do you want to play?\s
                    2 lies, 1 truth         -> enter lies\s
                    2 stupid 1 thought      -> enter stupid\s
                    Exit                    -> exit""");
            games = scanner.next();

            switch (games) {
                case "lies":

                    TwoLiesOneTruth.gameStart();
                    break;
                case "stupid":

                    TwoStupidOneThought.gameStart();
                    break;
                case "exit":
                    System.out.println("Thank you for playing our amazing games.");
                    isRunning = false;
            }
        } while (isRunning);
        scanner.close();
    }
}
