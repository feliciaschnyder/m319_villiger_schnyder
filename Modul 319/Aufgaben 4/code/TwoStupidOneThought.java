package ch.tbz.Template.Template.src.ch.tbz.lib;

import java.util.Objects;

import static ch.tbz.lib.Input.inputString;

public class TwoStupidOneThought {
    public static void gameStart() {

        TwoStupidOneThought stupidGame = new TwoStupidOneThought();

        System.out.println("Hello you have chosen the all mighty game of stupid people.\s" +
                "The Game will give, after you entered your name, a word. \s" +
                "An you job is it to write down the first word that comes you to mind in connection with the given word.\s" +
                "If you an you friends write down the same word, logical without cheating,\s" +
                " you earn as many points as people guessed the same word.\s" +
                "And now we wish you a lot of fun.\s" +
                "");

        String player1 = "";
        String player2 = "";
        String player3 = "";
        String menu = "";
        boolean isRunning = true;

        do {
            System.out.println("""
                    Menu:\s
                    enter a new player maximum 3-> player\s
                    start game                  -> start\s
                    leave the game :(           -> leave""");
            menu = HauptklasseA4.scanner.next();

            switch (menu) {
                case "player":
                    if (player1.equals("")) {
                        System.out.println("Whats your name player 1? (one word)");
                        player1 = HauptklasseA4.scanner.next();
                        System.out.println("Welcome" + player1 + " to the game.");
                    } else if (Objects.equals(player2, "")) {
                        System.out.println("Whats your name player 2? (one word)");
                        player2 = HauptklasseA4.scanner.next();
                        System.out.println("Welcome" + player2 + " to the game.");
                    } else if (Objects.equals(player3, "")) {
                        System.out.println("Whats your name player 3? (one word)");
                        player3 = HauptklasseA4.scanner.next();
                        System.out.println("Welcome" + player3 + " to the game.");
                    }
                    break;
                case "start":
                    stupidGame.game(player1, player2, player3);
                    System.out.println("Was the maximum points:");
                case "leave":
                    isRunning = false;
            }
        } while (isRunning);

    }

    public int game(String player1, String player2, String player3) {

        String[] words = {"fast food", "bike part", "sport clothes", "sport", "informatics", "computer",
                "school", "sun", "air", "pizza", "sweets", "game", "translator", "music app", "monkey", "pirate", "circus",
                "teacher", "mouse", "headphones", "phone", "virus", "lies", "ice tea", "browser", "lgbtq+", "hobby"};


        System.out.println("How many rounds do you want to play?");
        int rounds = HauptklasseA4.scanner.nextInt();
        String thoughtPlayer1 = "";
        String thoughtPlayer2 = "";
        String thoughtPlayer3 = "";
        int maxPoints;

        int pointsPlayer1 = 0;
        int pointsPlayer2 = 0;
        int pointsPlayer3 = 0;
        if (!Objects.equals(player2, "")) {
            for (int i = 1; i <= rounds; i++) {
                int index = (int) (Math.random() * (27 + 1));
                System.out.println(words[index]);
                if (!Objects.equals(player1, "")) {
                    thoughtPlayer1 = inputString("Whats your first thought " + player1 + " to " + words[index] + "?");

                    //for 30 empty lines
                    for (int j = 0; j <= 30; j++) {
                        System.out.println(" ");
                    }
                }
                if (!Objects.equals(player2, "")) {
                    thoughtPlayer2 = inputString("Whats your first thought " + player2 + " to " + words[index] + "?");

                    //for 30 empty lines
                    for (int j = 0; j <= 30; j++) {
                        System.out.println(" ");
                    }
                }
                if (!Objects.equals(player3, "")) {
                    thoughtPlayer3 = inputString("Whats your first thought " + player3 + " to " + words[index] + "?");

                    //for 30 empty lines
                    for (int j = 0; j <= 30; j++) {
                        System.out.println(" ");
                    }
                }


                //Points awarded per round
                if (Objects.equals(thoughtPlayer1, thoughtPlayer2)) {
                    if (Objects.equals(thoughtPlayer1, thoughtPlayer3)) {
                        pointsPlayer1 += 3;
                        pointsPlayer2 += 3;
                        pointsPlayer3 += 3;
                    } else {
                        pointsPlayer1 += 2;
                        pointsPlayer2 += 2;
                    }
                } else if (Objects.equals(thoughtPlayer2, thoughtPlayer3)) {
                    pointsPlayer3 += 2;
                    pointsPlayer2 += 2;
                }
            }

            // Point comparison to define winners
            if (pointsPlayer1 > pointsPlayer2) {
                if (pointsPlayer1 > pointsPlayer3) {
                    System.out.println("The Winner is: " + player1);
                    System.out.println("Points" + player1 + "-> " + pointsPlayer1);
                    System.out.println("Points" + player2 + "-> " + pointsPlayer2);
                    if (!Objects.equals(player3, "")) {
                        System.out.println("Points" + player3 + "-> " + pointsPlayer3);
                    }
                } else {
                    System.out.println("The Winner is: " + player3);
                    System.out.println("Points " + player1 + "-> " + pointsPlayer1);
                    System.out.println("Points " + player2 + "-> " + pointsPlayer2);
                    if (!Objects.equals(player3, "")) {
                        System.out.println("Points" + player3 + "-> " + pointsPlayer3);
                    }
                }

                //output for winner
            } else if (pointsPlayer1 == pointsPlayer2) {
                System.out.println("Its a tie.");
            } else if (pointsPlayer1 == pointsPlayer3) {
                System.out.println("Its a tie.");
            } else if (pointsPlayer2 == pointsPlayer3) {
                System.out.println("Its a tie.");
            } else if (pointsPlayer2 > pointsPlayer3) {
                System.out.println("The Winner is: " + player2);
                System.out.println("Points " + player1 + "-> " + pointsPlayer1);
                System.out.println("Points " + player2 + "-> " + pointsPlayer2);
                if (!Objects.equals(player3, "")) {
                    System.out.println("Points" + player3 + "-> " + pointsPlayer3);
                }
            } else {
                System.out.println("The Winner is: " + player3);
                System.out.println("Points " + player1 + "-> " + pointsPlayer1);
                System.out.println("Points " + player2 + "-> " + pointsPlayer2);
                if (!Objects.equals(player3, "")) {
                    System.out.println("Points " + player3 + "-> " + pointsPlayer3);
                }
            }
        } else {
            System.err.println("You need minimum 2 players!");
        }
        if (player3 != "") {
            maxPoints = rounds * 3 * 3;
        } else {
            maxPoints = rounds * 2 * 2;
        }
        return maxPoints;
    }
}
